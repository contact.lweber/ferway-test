/** @type {import('@types/eslint').Linter.BaseConfig} */
module.exports = {
  plugins: ['react-hooks', '@typescript-eslint', 'react', 'unused-imports'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'prettier',
  ],
  settings: {
    react: {
      version: 'detect',
    },
  },
  overrides: [
    {
      parserOptions: {
        tsconfigRootDir: __dirname,
        project: './tsconfig.json',
      },
      files: ['*.ts', '*.tsx', '*.js', '*.jsx'],
      rules: {
        'react/jsx-key': 'off',
        'react/jsx-uses-react': 'error',
        'react/jsx-uses-vars': 'error',
        'react/prop-types': 'off',
        'flowtype/no-types-missing-file-annotation': 'off',
        'import/no-unresolved': 'off',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        'react/jsx-props-no-spreading': 'off',
        'react-hooks/rules-of-hooks': 'error',
        'react-hooks/exhaustive-deps': 'warn',
        'no-return-await': 'off',
        'react/require-default-props': 'off',
        'react/react-in-jsx-scope': 'off',
        'no-unused-vars': 'off',
        '@typescript-eslint/no-unused-vars': ['error'],
        '@typescript-eslint/no-use-before-define': 'off',
        '@typescript-eslint/ban-ts-comment': 'off',

        'import/no-extraneous-dependencies': 'off',
      },
    },
  ],
}
