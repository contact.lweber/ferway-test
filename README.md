This is a [Next.js](https://nextjs.org/) project test for Ferway


## How to launch

First, close repository
Open term and 

``` bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result. 
(you can change port with `yarn dev -p 4300`).

You can search for station id and station name.
Results is displayed on box, with time until onboarding.
If duration of waiting is too long, time color change.

Basic feature with useEffect, useState, useRef etc...
No more lib needed here. (Like caching with useSWR or useQuery).
Project is initialised with typescript.
