import { Container, Stack } from "@chakra-ui/react";
import { Stations } from "../stations";
import { Schedules } from "../schedules";

export const Dashboard = () => {
  return (
    <Container maxW="1200px" p="20">
      <Stack spacing="10">
        <Stations />
        <Schedules />
      </Stack>
    </Container>
  );
};
