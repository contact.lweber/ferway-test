import { PropsWithChildren } from "react";
import {
  Box,
  Flex,
  Avatar,
  Link,
  Button,
  Menu,
  MenuButton,
  MenuList,
  useColorModeValue,
  Stack,
  Center,
  Text,
} from "@chakra-ui/react";

type NavLinkProps = PropsWithChildren & {
  href: string;
};

const NavLink = ({ children, href }: NavLinkProps) => (
  <Link
    px={2}
    py={1}
    rounded={"md"}
    _hover={{
      textDecoration: "none",
      bg: useColorModeValue("gray.200", "gray.700"),
    }}
    href={href}
  >
    {children}
  </Link>
);

export const Nav = () => {
  return (
    <>
      <Box bg={useColorModeValue("gray.100", "gray.900")} px={4}>
        <Flex h={16} alignItems="center" justifyContent="space-between">
          <Flex gap="10" alignItems="center">
            <Text
              textTransform="uppercase"
              fontWeight="extrabold"
              color="orange.600"
            >
              Ferway subway app
            </Text>
            <NavLink href="/historic">Historique</NavLink>
          </Flex>

          <Flex alignItems="center">
            <Stack direction="row" spacing={7}>
              <Menu>
                <MenuButton
                  as={Button}
                  rounded="full"
                  variant="link"
                  cursor="pointer"
                  minW={0}
                >
                  <Avatar size="sm" src="/logo.jpg" />
                </MenuButton>
                <MenuList alignItems="center">
                  <br />
                  <Center>
                    <Avatar size="2xl" src="/logo.jpg" />
                  </Center>
                  <br />
                  <Center>
                    <p>Bienvenue Séverin</p>
                  </Center>
                  <br />
                </MenuList>
              </Menu>
            </Stack>
          </Flex>
        </Flex>
      </Box>
    </>
  );
};
