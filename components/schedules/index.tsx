import {
  Alert,
  AlertDescription,
  AlertTitle,
  Box,
  HStack,
  SimpleGrid,
  Skeleton,
  Text,
} from "@chakra-ui/react";
import { faClock, faWarning } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/router";
import { useSchedules } from "../../lib/hooks";

export const Schedules = () => {
  const router = useRouter();

  const { schedules, schedulesLoading, errorSchedules } = useSchedules({
    stationName: router.query.stationName,
    stationId: router.query.stationId,
  });

  if (errorSchedules) {
    return (
      <Alert rounded="md" bgColor="red.300" status="error">
        <FontAwesomeIcon icon={faWarning} />
        <AlertTitle ml="3">Oups !</AlertTitle>
        <AlertDescription>
          Une erreur est survenue lors de la récupération des horaires.
        </AlertDescription>
      </Alert>
    );
  }

  if (schedulesLoading && !errorSchedules)
    return <Skeleton w="417px" h="95px" rounded="md" />;

  return (
    <SimpleGrid columns={[1, 1, 2, 3]} spacing="4">
      {schedules.map((schedule, index) => {
        const getTime = Number(schedule.message.replace(/\D+/g, ""));
        return (
          <Box
            position="relative"
            key={index}
            rounded="md"
            p="4"
            background="blackAlpha.400"
          >
            <HStack justifyContent="space-between" gap="10">
              <Text fontWeight="semibold">{schedule.destination}</Text>
              <HStack
                p="2"
                boxShadow="1px 2px 3px black"
                rounded="md"
                bgColor="blackAlpha.300"
                marginInlineStart="0"
              >
                <FontAwesomeIcon icon={faClock} size="sm" />
                <Text
                  color={
                    getTime < 3
                      ? "green.500"
                      : getTime >= 3 && getTime < 5
                      ? "orange.500"
                      : "red.500"
                  }
                  fontSize="xs"
                  fontWeight="extrabold"
                  whiteSpace="nowrap"
                >
                  {schedule.message}
                </Text>
              </HStack>
            </HStack>
          </Box>
        );
      })}
    </SimpleGrid>
  );
};
