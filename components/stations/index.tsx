import {
  Alert,
  AlertDescription,
  AlertTitle,
  SimpleGrid,
  Stack,
  Text,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useStations } from "../../lib/hooks";
import { Select } from "chakra-react-select";
import { STATIONS_NUMBER } from "../../lib/const/stations";
import { useEffect, useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWarning } from "@fortawesome/free-solid-svg-icons";

export const Stations = () => {
  const router = useRouter();
  const inputRefStation = useRef<any>(null);

  const [defaultStationValue, setDefaultStationValue] = useState<{
    value: number;
    label: string;
  }>();

  const { stations, errorStations } = useStations({
    stationId: router.query.stationId,
  });

  useEffect(() => {
    if (router.query.stationId)
      setDefaultStationValue(
        STATIONS_NUMBER[Number(router.query.stationId) - 1]
      );
  }, [router]);

  useEffect(() => {
    if (!router.query.stationName) inputRefStation.current?.clearValue();
  }, [router]);

  return (
    <>
      <SimpleGrid gap="4" columns={[1, 1, 2]}>
        <Stack flex={1}>
          <Text fontWeight="semibold">Numéro de la station :</Text>
          <Select
            value={{
              label: defaultStationValue?.label,
              value: defaultStationValue?.value,
            }}
            loadingMessage={() => "Chargement en cours..."}
            onChange={(option) => {
              router.push(`/?stationId=${option?.value}`, undefined, {
                shallow: true,
              });
            }}
            options={STATIONS_NUMBER}
            variant="filled"
          />
        </Stack>
        {errorStations ? (
          <Alert flex={1} status="error" rounded="md">
            <FontAwesomeIcon icon={faWarning} />
            <AlertTitle ml="3">Oups !</AlertTitle>
            <AlertDescription>
              Une erreur est survenue lors de la récupération des stations.
            </AlertDescription>
          </Alert>
        ) : (
          <Stack flex={1}>
            <Text fontWeight="semibold">Station :</Text>
            <Select
              ref={inputRefStation}
              loadingMessage={() => "Chargement en cours..."}
              onChange={(option) => {
                if (option)
                  router.replace({
                    query: { ...router.query, stationName: option.value },
                  });
              }}
              options={stations}
              variant="filled"
              placeholder="Sélectionnez une station"
            />
          </Stack>
        )}
      </SimpleGrid>
    </>
  );
};
