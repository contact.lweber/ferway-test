export const STATIONS_NUMBER = [
  {
    label: "Station 1",
    value: 1,
  },
  {
    label: "Station 2",
    value: 2,
  },
  {
    label: "Station 3",
    value: 3,
  },
  {
    label: "Station 4",
    value: 4,
  },
  {
    label: "Station 5",
    value: 5,
  },
  {
    label: "Station 6",
    value: 6,
  },
  {
    label: "Station 7",
    value: 7,
  },
  {
    label: "Station 8",
    value: 8,
  },
  {
    label: "Station 9",
    value: 9,
  },
  {
    label: "Station 10",
    value: 10,
  },
  {
    label: "Station 11",
    value: 11,
  },
  {
    label: "Station 12",
    value: 12,
  },
  {
    label: "Station 13",
    value: 13,
  },
  {
    label: "Station 14",
    value: 14,
  },
];
