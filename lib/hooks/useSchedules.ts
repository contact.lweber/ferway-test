import axios from "axios";
import { useCallback, useEffect, useState } from "react";
import { SchedulesType } from "../types/station";

type UseSchedulesProps = {
  stationName?: string | string[];
  stationId?: string | string[];
};

export const useSchedules = ({ stationId, stationName }: UseSchedulesProps) => {
  const [schedules, setSchedules] = useState<SchedulesType[]>([]);
  const [schedulesLoading, setSchedulesLoading] = useState<boolean>(false);
  const [errorSchedules, setErrorSchedules] = useState<boolean>(false);

  const fetchSchedules = useCallback(async () => {
    try {
      setSchedulesLoading(true);
      const { data } = await axios.get(
        `https://api-ratp.pierre-grimaud.fr/v4/schedules/metros/${stationId}/${stationName}/A+R`
      );

      if (data.result.schedules) {
        setErrorSchedules(false);
        setSchedulesLoading(false);
        setSchedules(data.result.schedules);
      }
    } catch (error) {
      setErrorSchedules(true);
    }
  }, [stationId, stationName]);

  useEffect(() => {
    if (stationName && stationId) fetchSchedules();
    else setSchedules([]);
  }, [fetchSchedules, stationId, stationName]);

  return { schedules, schedulesLoading, errorSchedules };
};
