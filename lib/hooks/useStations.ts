import axios from "axios";
import { GroupBase, OptionsOrGroups } from "chakra-react-select";
import { useCallback, useEffect, useState } from "react";
import { StationType } from "../types/station";

export type SelectStationType = {
  value: string;
  label: string;
};

type UseStationsType = {
  stationId?: string | number | string[];
};

export const useStations = ({ stationId = "1" }: UseStationsType) => {
  const [stations, setStations] = useState<
    OptionsOrGroups<SelectStationType, GroupBase<SelectStationType>> | undefined
  >(undefined);
  const [errorStations, setErrorStations] = useState(false);

  const fetchStations = useCallback(async () => {
    try {
      const { data } = await axios.get(
        `https://api-ratp.pierre-grimaud.fr/v4/stations/metros/${stationId}`
      );
      if (data.result.stations) {
        setStations(
          data.result.stations.map(({ name, slug }: StationType) => ({
            label: name,
            value: slug,
          }))
        );
      }
    } catch (error) {
      setErrorStations(true);
    }
  }, [stationId]);

  useEffect(() => {
    if (stationId) fetchStations();
  }, [fetchStations, stationId]);

  return { stations, errorStations };
};
