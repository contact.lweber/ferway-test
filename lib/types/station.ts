export type StationType = {
  name: string;
  slug: string;
};

export type SchedulesType = {
  message: string;
  destination: string;
};
