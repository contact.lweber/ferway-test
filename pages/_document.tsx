import Document, { Head, Html, Main, NextScript } from "next/document";

class MyDocument extends Document {
  private setMetasElements() {
    return (
      <>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="default" />
        <meta name="apple-mobile-web-app-title" content="Ferway subway test" />
        <meta name="application-name" content="Ferway subway test" />
        <meta name="description" content="Ferway subway test" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="mobile-web-app-capable" content="yes" />
      </>
    );
  }

  private setLinksElements() {
    return (
      <>
        <link href="https://fonts.googleapis.com" rel="preconnect" />
        <link href="https://fonts.gstatic.com" rel="preconnect" />

        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&display=swap"
          rel="stylesheet"
        />

        <link rel="shortcut icon" href="/favicon.ico" />
      </>
    );
  }

  render() {
    return (
      <Html>
        <Head>
          {this.setMetasElements()}
          {this.setLinksElements()}

          <noscript>
            An error occured, you either have JavaScript turned off or your
            browser doesn&apos;t support JavaScript
          </noscript>
          <noscript>
            Une erreur est survenue, soit JavaScript est désactivé, soit votre
            navigateur ne prend pas en charge JavaScript
          </noscript>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
