import type { NextPage } from "next";
import { Nav, Dashboard } from "../components";

const Home: NextPage = () => {
  return (
    <main>
      <Nav />
      <Dashboard />
    </main>
  );
};

export default Home;
